## INSTRUCTIONS

git clone git@gitlab.com:AleBurzio11/prova.git		clone repo with gitlab connection in current folder

git checkout -b <branch_name> 				to create new branch (and move inside)

git checkout <branch_name> 				to move to branch

git add <filename> 					after creating new file -> use '.' to add all files to local repo

git commit -m "message" 				to commit (save on local repo) changes and leave message

git push 						to push changes (send them to gitlab)

git diff						to view differences between what has been done and what has last been committed

git status 						to view current files that will be added/modified by committed

## MERGING BRANCHES

git checkout <branch_name> 				move to destination branch

git merge <branch_tomerge>				merge tomerge into current branch

(1)

git add .

git commit

git push

## IF THERE ARE CONFLICTS (1)

open the file that contains conflicts, remove conflicts



